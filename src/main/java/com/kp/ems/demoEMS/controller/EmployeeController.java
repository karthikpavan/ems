package com.kp.ems.demoEMS.controller;

import com.kp.ems.demoEMS.exception.ResourceNotFoundException;
import com.kp.ems.demoEMS.model.Employee;
import com.kp.ems.demoEMS.repository.EmployeeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {

  @Autowired
  EmployeeRepository employeeRepository;

  //will able to get all employee details
  @GetMapping
  public List<Employee> getALlEmployee() {
    return employeeRepository.findAll();
  }

  @PostMapping
  public Employee createEmployee(@RequestBody Employee employee) {
    return employeeRepository.save(employee);
  }

  @GetMapping("{id}")
  public ResponseEntity<Employee> getEmployeebyId(@PathVariable long id) {
    Employee employee = employeeRepository.findById(id).orElseThrow(
        () -> new ResourceNotFoundException("employee with id :" + id + " doesn't exists"));

    return ResponseEntity.ok(employee);
  }

  @PutMapping("{id}")
  public ResponseEntity<Employee> updateEmployee(@PathVariable long id,
      @RequestBody Employee updateEmpDetails) {
    Employee updateEmp = employeeRepository.findById(id).orElseThrow(
        () -> new ResourceNotFoundException("employee with id : " + id + " doesn't exists"));

    updateEmp.setFirstName(updateEmpDetails.getFirstName());
    updateEmp.setLastName(updateEmpDetails.getLastName());
    updateEmp.setEmail(updateEmpDetails.getEmail());
    employeeRepository.save(updateEmp);

    return ResponseEntity.ok(updateEmp);
  }

  @DeleteMapping("{id}")
  public ResponseEntity<HttpStatus> deleteEmployee(@PathVariable long id) {
    Employee deleteEmployee = employeeRepository.findById(id).orElseThrow(
        () -> new ResourceNotFoundException("Employee with id : " + id + " doesn't exists"));
    employeeRepository.delete(deleteEmployee);

    return new ResponseEntity(HttpStatus.NO_CONTENT);

  }
}
