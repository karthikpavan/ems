package com.kp.ems.demoEMS;

import com.kp.ems.demoEMS.model.Employee;
import com.kp.ems.demoEMS.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmsApplication implements CommandLineRunner {


  public static void main(String[] args) {
    SpringApplication.run(EmsApplication.class, args);
  }
  
  //done some changes here

  @Autowired
  EmployeeRepository employeeRepository;

  @Override
  public void run(String... args) throws Exception {

    Employee employee = new Employee();
    employee.setFirstName("Karthik");
    employee.setLastName("HY");
    employee.setEmail("hy@gmail.com");
    employeeRepository.save(employee);

    Employee employee1 = new Employee();
    employee1.setFirstName("Karthik");
    employee1.setLastName("pavan");
    employee1.setEmail("pavan@gmail.com");
    employeeRepository.save(employee1);
  }
}
