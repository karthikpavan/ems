package com.kp.ems.demoEMS.repository;

import com.kp.ems.demoEMS.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
